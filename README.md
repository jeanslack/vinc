# **Vinc** Is Not a Converter

UI for writing, adjustment and applying custom presets with FFmpeg.   


## Features

- Build your presets without limits, save your special profiles without worry.
- Multi-Platform, work on Linux, MacOs, Windows, FreeBsd.
- Drag n' Drop with multiple files at once.
- Batch processing for all loaded files.
- Fully customizable presets and profiles.
- Supports all formats and codecs available with FFmpeg.
- Has useful presets to start with.
- Peak level-based audio normalization.
- RMS-based audio normalization.
- EBU R128 audio normalization.
- Stream information
- Timeline for imported files, filters, export and tests.
- Log management.
- Multi language (English and Italian Languages support for now).

## Requirements
   
- Python3     
- wxPython4 (phoenix) 
- PyPubSub  
- ffmpeg >= 3.2   
- ffprobe (for multimedia streams analysis)  
- ffplay (for playback)   

