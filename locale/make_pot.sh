#!/bin/bash
# Make a *.po file to update translation (rename it vinc.pot)

CWD=$(pwd)


xgettext -d vinc "../vinc/Vinc.py" \
"../vinc/dialogs/dialog_tools.py" \
"../vinc/dialogs/ffmpeg_conf.py" \
"../vinc/dialogs/ffmpeg_codecs.py" \
"../vinc/dialogs/ffmpeg_formats.py" \
"../vinc/dialogs/ffmpeg_search.py" \
"../vinc/dialogs/first_time_start.py" \
"../vinc/dialogs/infoprg.py" \
"../vinc/dialogs/mediainfo.py" \
"../vinc/dialogs/presets_addnew.py" \
"../vinc/dialogs/settings.py" \
"../vinc/dialogs/shownormlist.py" \
"../vinc/dialogs/while_playing.py" \
"../vinc/inout/filedir_control.py" \
"../vinc/inout/IO_tools.py" \
"../vinc/inout/presets_manager_properties.py" \
"../vinc/main/main_frame.py" \
"../vinc/panels/dragNdrop.py" \
"../vinc/panels/presets_pan.py" \
"../vinc/process/ffplay_reproduction.py" \
"../vinc/process/ffplay_reproductionWin32.py" \
"../vinc/process/ffprobe_parser.py" \
"../vinc/process/ffprobe_parserWin32.py" \
"../vinc/process/task_processing.py" \
"../vinc/process/task_processingWin32.py" \
"../vinc/process/volumedetect.py" \
"../vinc/process/volumedetectWin32.py"


