��    �      �  =  �      �     �     �  (     	   1     ;     T     m     �     �      �      �  !   �                 i   &  ]   �     �               .     <  �   B  i   �  �   /     �  	   �     �  P   �  
   :  5   E  (   {  "   �     �     �  =   �  3        S     k     z  
   �     �     �     �  =   �     �          1  <   H     �  *   �     �  $   �            F   2  1   y     �     �  	   �     �     �                &  ;   2     n     v          �     �  $   �     �     �     �          +  �   2  �   �  �   z          '     8     =      S     t     �     �     �     �     �     �     �            7      N   !   g   ,   �   	   �   S   �      !  
   4!     ?!  "   V!     y!     �!     �!     �!  o   �!     "     #"  
   9"  /   D"  W   t"     �"     �"     �"  %   #     *#    8#  �   F$     �$     �$     %  1   %     G%     W%     h%     u%     �%  
   �%     �%     �%     �%     �%     �%     �%     &     &     .&     >&     [&     p&  ,   �&     �&     �&     �&     �&  )   �&  �  '     �)     �)     �)  0   �)  ;   !*     ]*     p*  !   �*     �*     �*  $   �*  !   �*  )   +     B+     I+     a+     y+     �+  #   �+     �+     �+  ?   �+  (   3,     \,     i,     o,     �,     �,  0   �,  >   �,  B   -     O-     d-     �-  "   �-  "   �-  &   �-  *   .     -.     J.     Q.     g.     p.     �.     �.     �.     �.     �.     �.  C   �.  n   5/  Q   �/  x   �/  �  o0  %   �1  
    2     +2     12     C2     P2     ^2     c2     o2     �2     �2     �2      �2     �2     �2  
   �2      �2      3  !   ,3     N3     l3     �3  x   �3  &   4  #   34  !   W4     y4     �4     �4     �4     �4     �4    �4     6     )6  /   F6  
   v6     �6  %   �6  '   �6     �6     �6  2   7  2   87  3   k7     �7     �7  
   �7  t   �7  h   28  1   �8     �8     �8     �8     9  �   9  �   �9  �   +:     �:     �:     �:  _   �:     P;  5   X;  .   �;  "   �;     �;     �;  M   �;  ;   J<  !   �<     �<     �<     �<  	   �<     �<     �<  Q   �<  &   N=  *   u=     �=  @   �=     �=  (   >  %   @>  &   f>     �>     �>  D   �>  2   ?     8?     W?     t?      �?  
   �?     �?     �?     �?  9   �?     @     !@  "   .@  	   Q@     [@  (   u@     �@  
   �@  %   �@      �@  
   �@  �   A  �   �A  �   rB  	   +C     5C     IC     NC  )   eC     �C     �C     �C     �C     �C     D      D     7D  )   QD     {D     �D  *   �D  "   �D  	   �D  e   E  -   lE     �E     �E  +   �E     �E     �E     F     F  |   2F     �F  +   �F     �F  ;   �F     5G     �G  +   �G     �G  '   	H     1H  4  EH  �   zI  !   DJ     fJ     xJ  8   �J     �J     �J     �J  .   �J     .K     :K     GK  3   OK     �K     �K  '   �K  '   �K  (   �K     %L     :L  '   LL  -   tL     �L  1   �L  !   �L     M  	   'M     1M  7   >M  �  vM  $   gP     �P     �P  7   �P  N   �P     MQ     iQ  ,   �Q  #   �Q     �Q  3   �Q  -   R  +   JR     vR     ~R  /   �R     �R      �R  '   S     .S     LS  ]   _S  )   �S     �S     �S     T     %T      5T  M   VT  Z   �T  J   �T  !   JU  $   lU      �U  -   �U  +   �U  .   V  3   ;V     oV     �V     �V     �V     �V     �V     �V     W     %W     8W     HW  G   YW  u   �W  H   X  �   `X  �  �X  -   �Z     �Z     �Z     �Z     [     [     0[     5[     A[     \[     b[  '   j[  (   �[     �[     �[  	   �[  &   �[  &   �[  '    \  %   H\     n\     �\  �   �\  /   %]  3   U]  %   �]  $   �]      �]     �]  "   ^     4^     T^                    l   B   '   Z   S      �       �   8   y   2   I   ^   c                   k   )   g       1               �   �   �   �   �   �       �       ?   T   �       �   	   p       �   &   #       r       �       �       �           �   �   �   ~       9       �   U   �   x   �   "   �   �   �   \         u   v   �       �   O   �   �      �       �   �      V   �          �   �   *   s       �   �      �           �   K       z   |   �   D   �   b      ]       �   A       �   �   !       �       �   /   j   �   �           %       _   �               �   �   �   q   Y      ,   �   
   �       P       w   �   �                  �   �   �               h          �   4   �   �   Q      H   �       0   �       L   M   t   �   �   e   �   �   E       �   N   ;   �   G   �           �           $                 �   }      -   �   �   :   o       �   �   J       m   i      �   `           �   (          R   �   @   =   �       �   5   �   �   �   �          .   �   C   �   �   7      �   �   �   a       �         d   [   W          �           �   6       �   �   3           �   �   �   �   �   �   �      �      {       n      �                      X       >   F   +      �      �   �   <   �   �       f        
  ...Not found 
  ..Nothing available 
  Choose a topic in the drop down first 
 Done !
 
 Interrupted Process !
 
 Sorry, tasks failed !
 
Wait....
Audio peak analysis.
  ...Completed
  ...Failed
  Use a custom path to run FFmpeg  Use a custom path to run FFplay  Use a custom path to run FFprobe &Help &Preferences &Tools '%s' is not installed on the system.
Please, install it or set a custom path using the 'Browse..' button. - Vinc is not a converter -
It provides a graphical interface to writing
presets for FFmpeg.
 ..Sometimes it can be useful =  Below max peak =  Clipped peaks =  No changes Abort Activate RMS-based normalization, which according to mean volume calculates the amount of gain to reach same average power signal. Activate peak level normalization, which will produce a maximum peak level equal to the set target level. Activate two passes normalization. It Normalizes the perceived loudness using the "​loudnorm" filter, which implements the EBU R128 algorithm. Add File..  Add Files All format streams An error was found in the search for the web page.
Sorry for this inconvenience. Appearance Are you sure you want to delete the selected profile? Assign a long description to the profile Assign a short name to the profile Audio Audio Normalization Audio normalization is not required based to set target level Audio normalization is required only for some files Audio normalization off Auto-detection Automations Bar Colour Browse.. Buttons Colour CODING ABILITY Can not find the configuration file

Sorry, cannot continue.. Change Buttons Font Colour: Change Toolbar Buttons Colour: Change Toolbar Colour: Changes will take affect once the program has been restarted Check new releases Choice a folder where save processed files Choose a Destination folder.. Choose one of the topics in the list Command line FFmpeg Configuration directory Create a new profile from yourself and save it in the selected preset. Create a new profile on the selected  preset "%s" Custom Colour UI DECODING CAPABILITY DISABLED: Delete the selected profile. Delete.. Demuxing only Demuxing/Muxing support Description Directories are not allowed, just add files, please: > '%s' Disable Disabled Don't show this dialog again Donation Drag some files here: Duplicate files are rejected: > '%s' Duration ENABLED: Edit profile on "%s" preset:  Edit the selected profile. Edit.. Enable custom search for the executable FFmpeg. If the checkbox is disabled or if the path field is empty, the search of the executable is entrusted to the system. Enable custom search for the executable FFplay. If the checkbox is disabled or if the path field is empty, the search of the executable is entrusted to the system. Enable custom search for the executable FFprobe. If the checkbox is disabled or if the path field is empty, the search of the executable is entrusted to the system. Enabled Executable paths Exit FFmpeg   ...installed FFmpeg   ...was imported locally FFmpeg decoders FFmpeg documentation FFmpeg encoders FFmpeg file formats FFmpeg search topics FFmpeg specifications FFplay   ...installed FFplay   ...not found ! FFplay   ...was imported locally FFprobe   ...installed FFprobe   ...not found ! FFprobe   ...was imported locally File does not exist or not a valid file:  %s File name File not found: '{0}'
'{1}' does not exist!

Need {2}

Please, choose a valid path. Files import with drag and drop Files list First pass parameters: First select a profile in the list Font Colour Format overview General General Settings Gets maximum volume and average volume data in dBFS, then calculates the offset amount for audio normalization. Help Hours amount duration Hours time If activated, hides all FFmpeg output messages. Ignore case distinctions, so that characters that differ only in case match each other. Ignore-case Incomplete profile assignments Informations Is 'ffmpeg' installed on your system? Issue tracker Length of cut missing: Cut (end point) [hh,mm,ss]

You should calculate the time amount between the Seeking (start point) and the cut point, then insert it in 'Cut (end point)'. Always consider the total duration of the flow in order to avoid entering incorrect values. Limiter for the maximum peak level or the mean level (when switch to RMS) in dBFS. From -99.0 to +0.0; default for PEAK level is -1.0; default for RMS is -20.0 Log View Console: Log directory Logging levels Make a back-up of the selected preset on combobox Max volume dBFS Mean volume dBFS Minutes Time Minutes amount duration Muxing only New Preset New.. No file selected to `%s` yet No files exported yet Offset dBFS Open path FFmpeg Open path FFplay Open path FFprobe Options disabled Options enabled PEAK-based volume statistics Path generation file Play selected file Play the file exported in the last encoding. Post-normalization references: Presets Manager Preview Profile Name Profile already stored with the same name Quiet.
Togle full screen.
Pause.
Togle mute.
Decrease and increase volume respectively.
Decrease and increase volume respectively.
Cycle audio channel in the current program.
Cycle video channel.
Cycle subtitle channel in the current program.
Cycle program.
Cycle video filters or show modes.
Step to the next frame. Pause if the stream is not 
already paused, step to the next video frame, and pause.
Seek backward/forward 10 seconds.
Seek backward/forward 1 minute.
Seek to the previous/next chapter. Or if there are no 
chapters Seek backward/forward 10 minutes.
Seek to percentage in file corresponding to fraction of width.
Toggle full screen. RMS-based volume statistics Reload presets list Remove the selected file Replace the selected preset with default values. Replace the selected preset with other saved custom preset. Reset all presets  Reset the current preset  Restore a previously saved preset Restore default settings Result dBFS Revert all presets to default values Save destination in source folder Save the current preset as separated file Save.. Second pass parameters: Seconds amount duration Seconds time Select a directory to save it Select a preset from the drop down: Selected profile name:  %s Set Icon Themes Set a global timeline to apply to any media file with duration. Set the number of threads (from 0 to 32) Settings CPU Setup Show Buttons Bar Show Metadata Show Tool Bar Show a dialog box to help you find FFmpeg topics Show dialog box with keyboard shortcuts useful during playback Show information about the metadata of the selected imported file. Show metadata window Show or hide buttons bar view Show tool bar view Shows available decoders on FFmpeg Shows available encoders on FFmpeg Shows file formats available on FFmpeg Shows the configuration features of FFmpeg Specific compilation options Starts Stops current process Subtitle Successful changes! Successful storing! Successfully saved Suppress excess output System options Target level: That's all folks! The search function allows you to find entries in the current topic The selected profile command has been changed manually.
Do you want to apply it during the conversion process? The selected profile is not suitable to convert the following file formats:

%s

 These settings affect output messages and the log
messages during processes. Change only if you
know what you are doing. This wizard will attempt to automatically detect FFmpeg in
your system.

In addition, it allows you to manually set a custom path
to locate FFmpeg and its associated executables.

Also, Remember that you can always change these settings
later, through the Setup dialog.

- Press 'Auto-detection' to start the system search now.

- Press 'Browse..' to indicate yourself where FFmpeg is located.
 Type the output format extension here User Guide Video Volume Statistics Volumedected While playing Wiki description duration is skipped flags format list sinks of the output device list sources of the input device names options parameters path to executable binary FFmpeg path to executable binary FFplay path to executable binary FFprobe print all options (very long) print basic options print more options q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

left/right
down/up
page down/page up

right mouse click
left mouse double-click show available HW acceleration methods show available audio sample formats show available bit stream filters show available color names show available devices show available filters show available pixel formats show available protocols status Project-Id-Version: Vinc v1.0.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-18 20:46+0200
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
Language-Team: Gianluca Pernigotto
X-Generator: Poedit 2.2.1
 
  ..Non trovato 
  ..Nessun dato disponibile 
  Scegli prima un argomento nel menu a discesa 
 Fatto !
 
 Processo Interrotto !
 
 Spiacenti, attività non riuscite!
 
Attendi....
Analisi livello di picco.
  ...Completato
  ...Fallito
  Usa un percorso personalizzato per avviare FFmpeg  Usa un percorso personalizzato per avviare FFplay  Usa un percorso personalizzato per avviare FFprobe &Aiuto &Preferenze &Strumenti '%s' non è installato nel sistema.
Per favore, installalo o imposta un percorso valido usando il tasto 'Sfoglia..'. - Vinc Is Not a Converter -
Vinc fornisce un'interfaccia grafica per la scrittura
di preset per FFmpeg.
 ...A volte può essere utile, ma usalo cautamente =  Sotto il picco max =  Picchi in clip =  Non cambia Interrompere Attiva la normalizzazione basata su RMS, che in base al volume medio calcola la quantità di guadagno per raggiungere la stessa potenza media di segnale. Attiva la normalizzazione del livello di picco, la quale produrrà un livello di picco massimo pari al livello target impostato. Esegue la normalizzazione a due passate. Normalizza il volume percepito usando il filtro "loudnorm", che implementa l'algoritmo EBU R128. Aggiungi File..  Aggiungi Files Tutti i flussi del formato E' avvenuto un errore inatteso nella ricerca della pagina web.
Mi dispiace per l'inconveniente. Aspetto Sei sicuro di voler eliminare il profilo selezionato? Assegnare una descrizione esaustiva al profilo Assegnare un nome breve al profilo Audio Normalizzazione Audio La normalizzazione audio non è richiesta in base al livello target impostato La normalizzazione audio è richiesta solo per alcuni files Normalizzazione Audio disattivata Auto-rileva Automazioni Colore Barra Sfoglia.. Colore Pulsanti ABILITÀ DI CODIFICA Impossibile trovare il file di configurazione.

Spiacente, non posso continuare.. Cambia il colore al font dei pulsanti: Cambia il colore dei pulsanti nella barra: Cambia il colore della barra: Le modifiche verranno applicate una volta riavviato il programma Controlla nuovi rilasci Cartella di destinazione file processati Scegli una cartella di destinazione.. Scegli uno degli argomenti nell'elenco Riga di Comando FFmpeg Directory di configurazione Crea un nuovo profilo da te stesso e salvalo nel preset selezionato. Crea un nuovo profilo sul preset selezionato "% s" Personalizza Il Colore Grafico CAPACITÀ DI DECODIFICAZIONE DISABILITATO: Cancella il profilo selezionato. Cancella.. Solo demuxing Demuxing e muxing Descrizione Le directory non sono ammesse, aggiungi solo file: > '%s' Disabilitato Disabilitato Non mostrare ancora questo dialogo Donazione Trascina alcuni file qui: I files duplicati sono rifiutati: > '%s' Durata ABILITATO: Modifica il profilo sul preset "%s":  Modifica il profilo selezionato. Modifica.. Abilita la ricerca personalizzata per il binario FFmpeg. Se la casella di controllo è disabilitata o se il campo percorso è vuoto, la ricerca dell'eseguibile è affidata al sistema. Abilita la ricerca personalizzata per il binario FFplay. Se la casella di controllo è disabilitata o se il campo percorso è vuoto, la ricerca dell'eseguibile è affidata al sistema. Abilita la ricerca personalizzata per il binario FFprobe. Se la casella di controllo è disabilitata o se il campo percorso è vuoto, la ricerca dell'eseguibile è affidata al sistema. Abilitato Percorsi eseguibili Esci FFmpeg   ...installato FFmpeg   ...E' stato importato localmente Decodificatori di FFmpeg Documentazione di FFmpeg Codificatori di FFmpeg Formati File di FFmpeg Argomenti di ricerca di FFmpeg Specifiche FFmpeg FFplay   ...installato FFplay   ...non trovato ! FFplay   ...E' stato importato localmente FFprobe   ...installato FFprobe   ...non trovato ! FFprobe   ...E' stato importato localmente File invalido o non esistente:  %s Nome file File non trovato: '{0}'
'{1}' non esiste!

Sono necessari {2}

Per favore, scegli un percorso valido. Finestra di importazione file con Drag n Drop Lista dei Files Parametri prima passata: Dovresti selezionare un profilo nella lista Colore Font Panoramica formato Generale Impostazioni del programma Ottiene i dati di volume massimo e volume medio in dBFS, quindi calcola la quantità di offset per la normalizzazione audio. Aiuto Tempo di durata in Ore, dal tempo di inizio Inizio del tempo in Ore Se attivato, nasconde tutti i messaggi di output di FFmpeg. Ignora le distinzioni tra maiuscole e minuscole, in modo che i caratteri che differiscono solo nel caso corrispondano tra loro. Ignora maiuscole/minuscole Le assegnazioni del profilo sono incomplete Informazioni 'ffmpeg' è installato sul tuo sistema? Segnala un Problema Lunghezza del taglio mancante: Taglia (punto finale) [ore, minuti, secondi]

È necessario calcolare la quantità di tempo tra la ricerca (punto iniziale) e il punto di taglio, quindi inserirlo in "Taglia (punto finale)". Considerare sempre la durata totale del flusso per evitare di immettere valori errati. Limitatore per il livello massimo di picco o il livello medio (quando si passa a RMS) in dBFS. Da -99,0 a +0,0; il valore predefinito per il livello PEAK è -1,0; il valore predefinito per RMS è -20,0 Console Visualizzazione Messaggi: Directory dei log Livelli logs Effettua un backup del preset selezionato sulla combobox Volume massimo dBFS Volume medio dBFS Inizio del tempo in Minuti Tempo di durata in Minuti, dal tempo di inizio Solo muxing Nuovo Preset Nuovo.. Ancora nessun file selezionato per la funzione `%s` Ancora nessun file esportato Offset dBFS Sfoglia e importa il percorso di FFmpeg Sfoglia e importa il percorso di FFplay Sfoglia e importa il percorso di FFprobe Opzioni disabilitate Opzioni abilitate Statistiche del volume basate sul PICCO Percorso importato per la generazione del log Riproduci il file selezionato Riproduci il file esportato nell'ultima codifica. Riferimenti post-normalizzazione: Gestionale Presets Anteprima Nome Profilo Questo profilo è già stato salvato con lo stesso nome Termina.
schermo intero.
Pausa.
Attiva / disattiva l'audio.
Ridurre e aumentare il volume rispettivamente.
Ridurre e aumentare il volume rispettivamente.
Ciclo del canale audio nel programma corrente.
Ciclo del canale video.
Scorri il canale dei sottotitoli nel programma corrente.
Programma ciclo.
Scorri i filtri video o mostra le modalità.
Passa al fotogramma successivo. Mette in pausa se lo stream non è
già in pausa, poi passa al fotogramma video successivo e rimette in pausa.
Cerca indietro/avanti di 10 secondi.
Cerca indietro/avanti di 1 minuto.
Cerca nel capitolo precedente/successivo. O se non ci sono 
capitoli Cerca indietro/avanti di 10 minuti.
Cerca la percentuale nel file corrispondente alla frazione di larghezza.
Schermo intero. Statistiche del volume basate su RMS Ricarica la lista dei presets Rimuovi il file selezionato Ripristina il preset corrente con i valori predefiniti. Importa e sostituisci con altri preset personalizzati precedentemente salvati. Ripristina tutti i presets  Ripristina il preset corrente  Ripristina un preset precedentemente salvato Ripristina impostazioni predefinite Risultato dBFS Ripristina tutti i presets con i valori predefiniti Salva la destinazione nella cartella sorgente Salva il preset corrente come file separato Salva.. Parametri seconda passata: Tempo di durata in Secondi, dal tempo di inizio Inizio del tempo in Secondi Salva nella cartella selezionata Seleziona un preset dal menu a discesa: Nome profilo selezionato:  %s Imposta Temi icona Imposta una sequenza temporale globale da applicare a qualsiasi file multimediale con durata. Imposta il numero dei threads (da 0 a 32) Impostazioni CPU Impostazioni Mostra/nascondi barra strumenti Mostra Metadati Mostra/Nascondi barra principale Mostra una finestra di dialogo per aiutarti a trovare gli argomenti di FFmpeg Mostra la finestra di dialogo con le scorciatoie da tastiera utili durante la riproduzione Mostra una finestra con i metadati del file selezionato nell'importazione. Mostra la finestra con i metadati Mostra o nascondi la barra strumenti Mostra/Nascondi barra Principale Mostra i decodificatori disponibili su FFmpeg Mostra i codificatori disponibili su FFmpeg Mostra i formati di file disponibili su FFmpeg Mostra le funzionalità di configurazione di FFmpeg Opzioni specifiche compilazione Avvio Interrompe il processo corrente Sottotitolo Modifiche riuscite! Profilo salvato con successo! Salvato con successo Sopprime l'output in eccesso Opzioni di sistema Livello target: Questo è tutto! La funzione di ricerca consente di trovare voci nell'argomento corrente Il comando del profilo selezionato è stato cambiato manualmente.
Vuoi applicarlo durante il processo di conversione? Il profilo selezionato è inadatto per i seguenti formati di file:

%s

 Queste impostazioni influiscono sui messaggi di output
e sui messaggi di log durante i processi. Modifica solo 
se sai cosa stai facendo. Questo assistente tenterà di individuare automaticamente
FFmpeg sul tuo sistema.

Inoltre, ti consente di impostare manualmente un percorso
specifico nel quale localizzare ffmpeg e gli eseguibili ad esso
associati.

Ricorda che è sempre possibile modificare queste impostazioni
anche in seguito, attraverso il dialogo di Setup.

- Premi 'Auto-rileva' per iniziare ora la ricerca sul sistema.

- Premi 'Sfoglia' per indicare tu stesso dove si trova FFmpeg.
 Scrivi qui l'estensione del formato in uscita Guida in linea Video Statistiche Volume Volumedected Durante la riproduzione Wiki descrizione la durata non è pervenuta flags formato elenca i sink del dispositivo di output elenca le fonti del dispositivo di input nomi opzioni parametri percorso del binario eseguibile FFmpeg percorso del binario eseguibile FFplay percorso del binario eseguibile FFprobe stampa tutte la opzioni (molto lungo) stampa le opzioni basilari stampa più opzioni q, ESC
f
p, SPC
m
9, 0
/, *
a
v
t
c
w
s

sinistra/destra
giù/su
pagina giù/pagina su

click destro del mouse
click sinistro del mouse mostra i metodi di accelerazione HW disponibili mostra i formati di campionamento audio disponibili mostra i filtri bitstream disponibili mostra i nomi dei colori disponibili mostra i dispositivi disponibili mostra i filtri disponibili mostra i pixel formats disponibili mostra i protocolli disponibili stato 