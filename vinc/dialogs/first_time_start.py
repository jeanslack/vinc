# -*- coding: UTF-8 -*-
#
#########################################################
# Name: first_time_start.py
# Porpose: Automatize settings first time start
# Compatibility: Python3, wxPython Phoenix
# Author: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2018/2019 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev: Dec 28 2018, Aug. 28 2109
#########################################################

# This file is part of Vinc.

#    Vinc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Vinc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with Vinc.  If not, see <http://www.gnu.org/licenses/>.

#########################################################

import wx
import os
from shutil import which
#---------------------------------------------------------------#

class FirstStart(wx.Dialog):
    """
    Shows a dialog wizard to locate FFmpeg executables
    """
    def __init__(self, img):
        """
        Set attribute with GetApp (see Vinc.py __init__)
        """
        get = wx.GetApp()
        self.FILEconf = get.FILEconf
        self.WORKdir = get.WORKdir
        self.OS = get.OS
        
        
        wx.Dialog.__init__(self, None, -1, style=wx.DEFAULT_DIALOG_STYLE)
        """constructor"""
        
        msg1 = (_(
            "This wizard will attempt to automatically detect FFmpeg in\n"
            "your system.\n\n"
            "In addition, it allows you to manually set a custom path\n"
            "to locate FFmpeg and its associated executables.\n\n"
            "Also, Remember that you can always change these settings\n"
            "later, through the Setup dialog.\n\n"
            "- Press 'Auto-detection' to start the system search now."
            "\n\n"
            "- Press 'Browse..' to indicate yourself where FFmpeg is located.\n")
               )
        # widget:
        bitmap_drumsT = wx.StaticBitmap(self, wx.ID_ANY, wx.Bitmap(
                                        img,wx.BITMAP_TYPE_ANY))
        lab_welc1 = wx.StaticText(self, wx.ID_ANY, (
                                        _("Welcome to Vinc Wizard!")))
        lab_welc2 = wx.StaticText(self, wx.ID_ANY, (msg1))
        
        self.detectBtn = wx.Button(self, wx.ID_ANY, (_("Auto-detection")))

        self.browseBtn = wx.Button(self, wx.ID_ANY, (_("Browse..")))
        
        close_btn = wx.Button(self, wx.ID_EXIT, "")
        
        # properties
        self.SetTitle(_("Vinc: Wizard"))
        lab_welc1.SetFont(wx.Font(11, wx.DEFAULT, wx.NORMAL,wx.BOLD, 0, ""))
        # layout:
        sizer_base = wx.BoxSizer(wx.VERTICAL)
        grd_base = wx.FlexGridSizer(2, 1, 0, 0)
        grd_1 = wx.FlexGridSizer(1, 2, 0, 0)
        grd_ext = wx.FlexGridSizer(4, 1, 0, 0)
        grd_2 = wx.FlexGridSizer(3, 2, 0, 0)
        grd_base.Add(grd_1)
        grd_1.Add(bitmap_drumsT,0,wx.ALL, 10)
        grd_1.Add(grd_ext)
        grd_base.Add(grd_2)
        grd_ext.Add(lab_welc1,0,  wx.ALL, 10)
        grd_ext.Add(lab_welc2,0, wx.ALIGN_CENTER | wx.ALL, 10)
        grd_2.Add(self.detectBtn,0, wx.ALL, 15)

        grd_2.Add((260,0), 0, wx.ALL, 5)
        grd_2.Add(self.browseBtn,0, wx.ALL, 15)

        grd_2.Add((260,0), 0, wx.ALL, 5)
        grd_btn = wx.FlexGridSizer(1, 2, 0, 0)
        
        grd_btn.Add(close_btn,0, flag=wx.ALL, border=5)
        grd_2.Add((260,0), 0, wx.ALL, 15)
        grd_2.Add(grd_btn,0, flag=wx.ALL|wx.ALIGN_RIGHT|wx.RIGHT, border=10)
        #properties
        self.detectBtn.SetMinSize((200, -1))
        self.browseBtn.SetMinSize((200, -1))
        
        sizer_base.Add(grd_base)
        self.SetSizer(sizer_base)
        sizer_base.Fit(self)
        self.Layout()
        
        ######################## bindings #####################
        self.Bind(wx.EVT_BUTTON, self.On_close)
        self.Bind(wx.EVT_BUTTON, self.Detect, self.detectBtn)
        self.Bind(wx.EVT_BUTTON, self.Browse, self.browseBtn)
        self.Bind(wx.EVT_CLOSE, self.On_close) # controlla la chiusura (x)
        
    # EVENTS:
    #-------------------------------------------------------------------#
    def On_close(self, event):
        self.Destroy()
    #-------------------------------------------------------------------#
    def Browse(self, event):
        """
        The user find and import FFmpeg executables folder with
        ffmpeg, ffprobe, ffplay inside on Posix or ffmpeg.exe, ffprobe.exe, 
        ffplay.exe inside on Windows NT.
        """
        
        if self.OS == 'Windows':
            listFF = {'ffmpeg.exe':"",'ffprobe.exe':"",'ffplay.exe':""}
        else:
            listFF = {'ffmpeg':"",'ffprobe':"",'ffplay':""}
            
        dirdialog = wx.DirDialog(self, 
                _("Vinc: locate the ffmpeg folder"), "", 
                wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST
                                )
            
        if dirdialog.ShowModal() == wx.ID_OK:
            path = "%s" % dirdialog.GetPath()
            dirdialog.Destroy()
            
            filelist = []
            for ff in os.listdir(path):
                if ff in listFF.keys():
                    listFF[ff] = os.path.join("%s" % path, "%s" % ff)
                    
            error = False
            for key,val in listFF.items():
                if not val:
                    error = True
                    break
            if error:
                wx.MessageBox(_("File not found: '{0}'\n"
                                "'{1}' does not exist!\n\n"
                                "Need {2}\n\n"
                                "Please, choose a valid path.").format(
                                        os.path.join("%s" % path, "%s" % key),
                                        key,
                                        [k for k in listFF.keys()]), 
                                "Vinc: warning!",  wx.ICON_WARNING, self
                                )
                return
            
            self.completion([v for v in listFF.values()])

    #-------------------------------------------------------------------#
    def Detect(self, event):
        """
        Check for dependencies into your system (compatible with Linux, 
        MacOsX, Windows)
        [https://stackoverflow.com/questions/11210104/check-if-a-program-exists-
        from-a-python-script]
        Search the executable in the system, if fail stop the search, 
        otherwise write the executable pathname in the configuration file.
        """
        local = False
        if self.OS == 'Windows':
            biname = ['ffmpeg.exe','ffprobe.exe','ffplay.exe']
        else:
            biname = ['ffmpeg','ffprobe','ffplay']
            
        for required in biname:
            if which(required):
                print ("Check for: '%s' ..Ok" % required)
                no_which = False
            else:
                print ("Check for: '%s' ..Not Installed" % required)
                if self.OS == 'Darwin':
                    if os.path.isfile("/usr/local/bin/%s" % required):
                        local = True
                        no_which = False
                        break
                    else:
                        local = False
                        no_which = True
                        break
                elif self.OS == 'Windows':
                    no_which = True
                    break
        if no_which:
            for x in biname:
                if not os.path.isfile("%s/FFMPEG_BIN/bin/%s" %(self.WORKdir, x)):
                    noexists = True
                    break
                else:
                    noexists = False
            if noexists:
                wx.MessageBox(_("'%s' is not installed on the system.\n"
                          "Please, install it or set a custom path "
                          "using the 'Browse..' button.") 
                          % required, 'Vinc: Warning', 
                          wx.ICON_EXCLAMATION, self)
                return
            else:
                if wx.MessageBox(_("Vinc already seems to include "
                                   "FFmpeg and the executables associated "
                                   "with it.\n\n"
                                   "Are you sure you want to use them?"), 
                        _('Vinc: Please Confirm'),
                        wx.ICON_QUESTION |
                        wx.YES_NO, 
                        None) == wx.YES:
                    ffmpeg = "%s/FFMPEG_BIN/bin/%s" % (self.WORKdir, biname[0])
                    ffprobe = "%s/FFMPEG_BIN/bin/%s" % (self.WORKdir, biname[1])
                    ffplay = "%s/FFMPEG_BIN/bin/%s" % (self.WORKdir, biname[2])
                else:
                    return
        else:
            if local:
                ffmpeg = "/usr/local/bin/ffmpeg"
                ffprobe = "/usr/local/bin/ffprobe"
                ffplay = "/usr/local/bin/ffplay"
            else:
                ffmpeg = which(biname[0])
                ffprobe = which(biname[1])
                ffplay = which(biname[2])
        
        self.completion([ffmpeg,ffprobe,ffplay])
    #-------------------------------------------------------------------#
    
    def completion(self, FFmpeg):
        """
        Writes changes to the configuration file
        """
        ffmpeg = FFmpeg[0]
        ffprobe = FFmpeg[1]
        ffplay = FFmpeg[2]
        rowsNum = []#rows number list
        dic = {} # used for debug
        with open (self.FILEconf, 'r') as f:
            full_list = f.readlines()
        for a,b in enumerate(full_list):
            if not b.startswith('#'):
                if not b == '\n':
                    rowsNum.append(a)

        full_list[rowsNum[6]] = '%s\n' % ffmpeg
        full_list[rowsNum[8]] = '%s\n' % ffprobe
        full_list[rowsNum[10]] = '%s\n' % ffplay
        
        with open (self.FILEconf, 'w') as fileconf:
            for i in full_list:
                fileconf.write('%s' % i)
            
        wx.MessageBox(_("\nWizard completed successfully.\n"
                       "Restart Vinc now.\n\nThank You!"), 
                       _("That's all folks!"))   
        self.Destroy()
        
