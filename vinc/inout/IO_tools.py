# -*- coding: UTF-8 -*-

#########################################################
# Name: streams_tools.py
# Porpose: Redirect any input/output resources to process
# Compatibility: Python3, wxPython4 Phoenix
# Author: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2018/2019 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev Dec.28.2018, Sept.19.2019
#########################################################

# This file is part of Vinc.

#    Vinc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Vinc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with Vinc.  If not, see <http://www.gnu.org/licenses/>.

#########################################################

import wx

get = wx.GetApp()
OS = get.OS
DIRconf = get.DIRconf

if OS == 'Windows':
    from vinc.process.long_processingWin32 import Progress_Processes
    from vinc.process.volumedetectWin32 import VolumeDetectThread
    from vinc.process.volumedetectWin32 import PopupDialog
    from vinc.process.ffplay_reproductionWin32 import Play
    from vinc.process.ffprobe_parserWin32 import FFProbe
else:
    from vinc.process.long_processing import Progress_Processes
    from vinc.process.volumedetect import VolumeDetectThread
    from vinc.process.volumedetect import PopupDialog
    from vinc.process.ffplay_reproduction import Play
    from vinc.process.ffprobe_parser import FFProbe
    
    
from vinc.dialogs.mediainfo import Mediainfo
from vinc.process.check_bin import ff_conf
from vinc.process.check_bin import ff_formats
from vinc.process.check_bin import ff_codecs
from vinc.process.check_bin import ff_topics
from vinc.process.opendir import browse
from vinc.dialogs import ffmpeg_conf
from vinc.dialogs import ffmpeg_formats
from vinc.dialogs import ffmpeg_codecs

#-----------------------------------------------------------------------#
def process(self, varargs, panelshown, duration, time_seq, time_read):
    """
    1) TIME DEFINITION FOR THE PROGRESS BAR
        For a suitable and efficient progress bar, if a specific 
        time sequence has been set with the duration tool, the total 
        duration of each media file will be replaced with the set time 
        sequence. Otherwise the duration of each media will be the one 
        originated from its real duration.
        
    2) STARTING THE PROCESS
        Here the panel with the progress bar is instantiated which will 
        assign a corresponding thread.
        
    """
    if time_seq:
        newDuration = []
        for n in duration:
            newDuration.append(time_read['time'][1])
        duration = newDuration
        
    self.ProgressPanel = Progress_Processes(self, 
                                           panelshown, 
                                           varargs, 
                                           duration, 
                                           OS,
                                           )
#-----------------------------------------------------------------------#
def stream_info(title, filepath , ffprobe_link):
    """
    Show media information of the streams content.
    This function make a bit control of file existance.
    """
    try:
        with open(filepath):
            dialog = Mediainfo(title, 
                               filepath, 
                               ffprobe_link, 
                               OS,
                               )
            dialog.Show()

    except IOError:
        wx.MessageBox(_("File does not exist or not a valid file:  %s") % (
            filepath), "Vinc: warning", wx.ICON_EXCLAMATION, None)
        
#-----------------------------------------------------------------------#
def stream_play(filepath, timeseq, ffplay_link, param, loglevel_type):
    """
    Thread for media reproduction with ffplay
    """
    try:
        with open(filepath):
            thread = Play(filepath, 
                          timeseq, 
                          ffplay_link, 
                          param, 
                          loglevel_type, 
                          OS,
                          )
            #thread.join()# attende che finisca il thread (se no ritorna subito)
            #error = thread.data
    except IOError:
        wx.MessageBox(_("File does not exist or not a valid file:  %s") % (
            filepath), "Vinc: warning", wx.ICON_EXCLAMATION, None)
        return
    
#-----------------------------------------------------------------------#
def probeDuration(path_list, ffprobe_link):
    """
    Parsa i metadati riguardanti la durata (duration) dei media importati, 
    utilizzata per il calcolo della progress bar. Questa funzione viene 
    chiamata nella MyListCtrl(wx.ListCtrl) con il pannello dragNdrop.
    Se i file non sono supportati da ffprobe e quindi da ffmpeg, avvisa
    con un messaggio di errore.
    """
    metadata = FFProbe(path_list, ffprobe_link, 'no_pretty') 
        # first execute a control for errors:
    if metadata.ERROR():
        err = metadata.error
        print ("[FFprobe] Error:  %s" % err)
        #wx.MessageBox("%s\nFile not supported!" % (metadata.error),
                      #"Error - Vinc", 
                      #wx.ICON_ERROR, None)
        duration = 0
        return duration, err
    try:
        for items in metadata.data_format()[0]:
            if items.startswith('duration='):
                duration = (int(items[9:16].split('.')[0]))
    except ValueError as ve:
        duration = 0
        if ve.args[0] == "invalid literal for int() with base 10: 'N/A'":
            return duration, 'N/A'
        else:
            return duration, ve.args
    
    return duration , None
#-------------------------------------------------------------------------#
def volumeDetectProcess(ffmpeg, filelist, time_seq):
    """
    Run a thread to get audio peak level data and show a 
    pop-up dialog with message. 
    """
    thread = VolumeDetectThread(ffmpeg, time_seq, filelist, OS) 
    loadDlg = PopupDialog(None, _("Vinc - Loading..."), 
                                _("\nWait....\nAudio peak analysis.\n")
                          )
    loadDlg.ShowModal()
    #thread.join()
    data = thread.data
    loadDlg.Destroy()
    
    return data
#-------------------------------------------------------------------------#
def test_conf(ffmpeg_link, ffprobe_link, ffplay_link):
    """
    Call *check_bin.ffmpeg_conf* to get data to test the building 
    configurations of the installed or imported FFmpeg executable 
    and send it to dialog box.
    
    """
    out = ff_conf(ffmpeg_link, OS)
    if 'Not found' in out[0]:
        wx.MessageBox("\n{0}".format(out[1]), 
                        "Vinc: error",
                        wx.ICON_ERROR, 
                        None)
        return
    else:
        dlg = ffmpeg_conf.Checkconf(out, 
                                    ffmpeg_link, 
                                    ffprobe_link,
                                    ffplay_link, 
                                    OS,
                                    )
        dlg.Show()
#-------------------------------------------------------------------------#
def test_formats(ffmpeg_link):
    """
    Call *check_bin.ff_formats* to get available formats by 
    imported FFmpeg executable and send it to dialog box.
    
    """
    diction = ff_formats(ffmpeg_link, OS)
    if 'Not found' in diction.keys():
        wx.MessageBox("\n{0}".format(diction['Not found']), 
                        "Vinc: error",
                        wx.ICON_ERROR, 
                        None)
        return
    else:
        dlg = ffmpeg_formats.FFmpeg_formats(diction, OS)
        dlg.Show()
#-------------------------------------------------------------------------#
def test_codecs(ffmpeg_link, type_opt):
    """
    Call *check_bin.ff_codecs* to get available encoders 
    and decoders by FFmpeg executable and send it to
    corresponding dialog box.
    
    """
    diction = ff_codecs(ffmpeg_link, type_opt, OS)
    if 'Not found' in diction.keys():
        wx.MessageBox("\n{0}".format(diction['Not found']), 
                        "Vinc: error",
                        wx.ICON_ERROR, 
                        None)
        return
    else:
        dlg = ffmpeg_codecs.FFmpeg_Codecs(diction, OS, type_opt)
        dlg.Show()
#-------------------------------------------------------------------------#
def findtopic(ffmpeg_link, topic):
    """
    Call * check_bin.ff_topic * to run the ffmpeg command to search
    a certain topic. The ffmpeg_link is given by ffmpeg-search dialog.
    
    """
    retcod = ff_topics(ffmpeg_link, topic, OS)
    
    if 'Not found' in retcod[0]:
        s = ("\n{0}".format(retcod[1]))
        return(s)
    else:
        return(retcod[1])
#-------------------------------------------------------------------------#
def openpath(mod):
    """
    Call process.opendir.browse
    
    """
    ret = browse(OS, DIRconf, mod)
    if ret:
        wx.MessageBox(ret, 'Vinc', wx.ICON_ERROR, None)
#-------------------------------------------------------------------------#


