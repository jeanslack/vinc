# -*- coding: UTF-8 -*-

#########################################################
# File Name: make_filelog.py
# Porpose: Module to generate log files durin jobs start
# Compatibility: Python3, Python2
# Author: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) 2018/2019 Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Rev: Dec.28 2018, Aug.29 2019
#########################################################

# This file is part of Vinc.

#    Vinc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Vinc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with Vinc.  If not, see <http://www.gnu.org/licenses/>.

#########################################################

import time
import os

def write_log(logname, dirconf):
    """
    During the process, it write log about what the program does,
    command output and including errors.
    The log is stored in 'vinc/log' by default.

    - logname è il nome del pannello da cui è stato composto il comando
    """
    if not os.path.isdir(dirconf):
        try:
            os.mkdir(dirconf)
        except OSError as e:
            print(e)
            print ("Directory creation failed '%s'" % dirconf)
            
    current_date =  time.strftime("%c") # date/time

    with open("%s/%s" % (dirconf, logname),"w") as log:
        log.write("""[PYTHON] CURRENT DATE/TIME:
%s\n
-----------------------------------------
[VINC] INFO FOR USERS: 
-----------------------------------------
All FFmpeg and FFplay output messages are on stderr (excluse ffprobe), 
and include both information messages and error messages.
Changing the logging level into setting dialog would also change the 
behavior of the output on log messages.
-----------------------------------------
On Vinc default ffmpeg loglevel is fixed to 'warning';
ffplay to 'error' .
For more details, see vinc.conf or vincWin32.conf 
into configuration directory.
-----------------------------------------

[VINC] COMMAND LINE:

""" % (current_date))
